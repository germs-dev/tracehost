`tracehost` is a bash script that takes a hosts file on `stdin`, pings each
host and generates a Graphviz network topology of all the IPs encountered on
the way. Fork [this repo](https://gitlab.com/germs-dev/tracehost), edit
`hosts` and run `make` to generate this SVG.

[![](hosts.svg)](hosts.svg)
