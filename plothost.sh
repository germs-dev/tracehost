#!/bin/bash

echo -e "strict graph {\nnode [color=\"white\"]\n"

for file in public/*.txt; do

	# Get contents of file
	mapfile hops < $file

	# Is the target responding to pings?
	ping -w 1 -c 1 $target >& /dev/null && color=black || color=red
	echo -e "\n\"$target\" [fontcolor=\"$color\"]"

	# Dump hops that contain a hostname or IP
	echo "\"$(hostname -I)\" -- "
	for hop in "${hops[@]}"; do
		[[ $hop =~ [^\ ]+\.[^\ ]+ ]] && echo -en "\"$BASH_REMATCH\" -- "
	done

	# Finish off with the destination, strip path and extension
	target=$(basename $file)
	target=${target/\.txt}
	echo "\"$target\""
done

echo "}"
