#!/bin/bash

mapfile hosts

for entry in "${hosts[@]}"; do

	# Extract host
	if [[ $entry =~ ^[[:graph:]]+ ]]; then

		host=$BASH_REMATCH

		if [[ ! $host =~ \# ]]; then
			echo Entry \"$entry\"
			echo Probing \"$host\"

			# Location for trace results - trim any spaces
			output_file=public/$host.txt

			echo -e "\nWriting trace to $output_file"

			# Run tracepath and write the results to a file
			tracepath -m 15 -b -4 $host |& tee $output_file
		else
			echo Ignoring 1 \"$entry\"
		fi

	else
		echo Ignoring 2 \"$entry\"
	fi
done
